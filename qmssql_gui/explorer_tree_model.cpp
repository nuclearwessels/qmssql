

#include "explorer_tree_model.h"

ExplorerTreeItem::ExplorerTreeItem(ExplorerItemType type, const QVector<QVariant>& data, ExplorerTreeItem* parent) :
    _type(type),
    _parent(parent),
    _data(data)
{

}


ExplorerTreeItem::~ExplorerTreeItem()
{
    qDeleteAll(_children);
}


ExplorerItemType ExplorerTreeItem::type() const
{
    return _type;
}


QVariant ExplorerTreeItem::data(int column) const
{
    if (column < 0 || column > _data.size())
        return QVariant();

    return _data.at(column);
}


ExplorerTreeItem* ExplorerTreeItem::child(int row) const
{
    if (row < 0 || row >= _children.count())
    {
        return nullptr;
    }

    return _children.at(row);
}


int ExplorerTreeItem::childCount() const
{
    return _children.count();
}

int ExplorerTreeItem::columnCount() const
{
    return _data.count();
}

int ExplorerTreeItem::row() const
{
    if (_parent)
    {
        return _parent->_children.indexOf(const_cast<ExplorerTreeItem*>(this));
    }

    return 0;
}


ExplorerTreeItem* ExplorerTreeItem::parent()
{
    return _parent;
}



void ExplorerTreeItem::addChild(ExplorerTreeItem* child)
{
    _children.append(child);
}


ExplorerTreeModel::ExplorerTreeModel(QObject* parent) :
    QAbstractItemModel(parent)
{

    _rootItem = new ExplorerTreeItem(ExplorerItemType::ROOT_ITEM,
                                     {"Databases"});

    ExplorerTreeItem* db1 = new ExplorerTreeItem(ExplorerItemType::DB_NAME,
                                                 {"DB1"},
                                                 _rootItem);
    _rootItem->addChild(db1);

    ExplorerTreeItem* tables1 = new ExplorerTreeItem(ExplorerItemType::TABLES,
                                                     {"Tables"},
                                                     db1);
    db1->addChild(tables1);

}


ExplorerTreeModel::~ExplorerTreeModel()
{
    delete _rootItem;
}


void ExplorerTreeModel::addDatabase(const Database& dbObj)
{
//    ExplorerTreeItem* db = new ExplorerTreeItem(ExplorerItemType::DB_NAME, dbObj.name());

//    ExplorerTreeItem* table = new ExplorerTreeItem(ExplorerItemType::TABLES,
//                                                   {"Tables"},
//                                                   db);

//    _rootItem->addChild(db);


}


QModelIndex ExplorerTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
    {
        return QModelIndex();
    }

    ExplorerTreeItem* parentItem = nullptr;


    if (!parent.isValid())
    {
        parentItem = _rootItem;
    }
    else
    {
        parentItem = static_cast<ExplorerTreeItem*>(parent.internalPointer());
    }

    ExplorerTreeItem* childItem = parentItem->child(row);
    if (childItem)
    {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}


QModelIndex ExplorerTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return QModelIndex();
    }

    ExplorerTreeItem* childItem = static_cast<ExplorerTreeItem*>(index.internalPointer());
    ExplorerTreeItem* parentItem = childItem->parent();

    if (parentItem == _rootItem)
    {
        return QModelIndex();
    }

    return createIndex(parentItem->row(), 0, parentItem);
}


int ExplorerTreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
    {
        return 0;
    }

    ExplorerTreeItem* parentItem = nullptr;

    if (!parent.isValid())
    {
        parentItem = _rootItem;
    }
    else
    {
        parentItem = static_cast<ExplorerTreeItem*>(parent.internalPointer());
    }

    return parentItem->childCount();
}


int ExplorerTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return static_cast<ExplorerTreeItem*>(parent.internalPointer())->columnCount();
    }

    return _rootItem->columnCount();
}


QVariant ExplorerTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (role != Qt::DisplayRole)
    {
        return QVariant();
    }

    ExplorerTreeItem* item = static_cast<ExplorerTreeItem*>(index.internalPointer());

    QVariant data = item->data(index.column());

    return data;
}


Qt::ItemFlags ExplorerTreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}


QVariant ExplorerTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        return _rootItem->data(section);
    }

    return QVariant();
}


