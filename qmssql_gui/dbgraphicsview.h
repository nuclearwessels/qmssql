#ifndef DBGRAPHICSVIEW_H
#define DBGRAPHICSVIEW_H

#include <QGraphicsItem>
#include <QGraphicsView>

#include <QWheelEvent>


class DbGraphicsColumn : public QGraphicsSimpleTextItem
{
public:
    DbGraphicsColumn(QGraphicsItem* parent, const QString& columnName);

    int type() const override;

private:
    QString columnName_;
};


class DbGraphicsTableHeader : public QGraphicsSimpleTextItem
{
public:
    DbGraphicsTableHeader(QGraphicsItem* parent, const QString& tableName);

    int type() const override;

private:
    QString tableName_;
};


class DbGraphicsTable : public QGraphicsItemGroup
{
public:
    DbGraphicsTable(QGraphicsItem* parent, const QString& name, const QList<QString>& columns);

    int type() const override;

protected:


private:
    QString name_;
    QGraphicsSimpleTextItem* nameHeader_;
    QGraphicsSimpleTextItem* tableHeader_;

    QList<DbGraphicsColumn*> columnHeaders_;
};



class DbGraphicsView : public QGraphicsView
{
public:
    DbGraphicsView(QWidget* parent);

protected:
    void contextMenuEvent(QContextMenuEvent* cmu) override;
    void wheelEvent(QWheelEvent* we) override;
};

#endif // DBGRAPHICSVIEW_H
