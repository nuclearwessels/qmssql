
#include "dbgraphicsview.h"

#include <QMenu>
#include <qmath.h>


const int COLUMN_TYPE = QGraphicsItem::UserType + 1;
const int TABLE_TYPE = QGraphicsItem::UserType + 2;
const int TABLE_HEADER_TYPE = QGraphicsItem::UserType + 3;


DbGraphicsColumn::DbGraphicsColumn(QGraphicsItem* parent, const QString& columnName) :
    QGraphicsSimpleTextItem(columnName, parent),
    columnName_(columnName)
{
    setFlag(QGraphicsItem::ItemIsFocusable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
}


int DbGraphicsColumn::type() const
{
    return COLUMN_TYPE;
}


DbGraphicsTableHeader::DbGraphicsTableHeader(QGraphicsItem* parent, const QString& tableName) :
    QGraphicsSimpleTextItem(tableName, parent),
    tableName_(tableName)
{

}


int DbGraphicsTableHeader::type() const
{
    return TABLE_HEADER_TYPE;
}


DbGraphicsTable::DbGraphicsTable(QGraphicsItem* parent, const QString& name, const QList<QString>& columns) :
    QGraphicsItemGroup(parent),
    name_(name)
{
    setFlag(QGraphicsItem::ItemIsMovable, true);

    tableHeader_ = new DbGraphicsTableHeader(this, "Columns:");
    addToGroup(tableHeader_);

    qreal pos = 0.0;

    QRectF tableHeaderRect = tableHeader_->boundingRect();

    pos += tableHeaderRect.size().height();

    for (const QString& col : columns)
    {
        DbGraphicsColumn* colItem = new DbGraphicsColumn(this, col);
        colItem->setPos(tableHeaderRect.x(), pos);

        QRectF rect = colItem->boundingRect();
        pos += rect.height();
    }
}


int DbGraphicsTable::type() const
{
    return TABLE_TYPE;
}


DbGraphicsView::DbGraphicsView(QWidget* parent) :
    QGraphicsView(parent)
{

}


void DbGraphicsView::contextMenuEvent(QContextMenuEvent *cme)
{
    // find the object
    QPointF scenePt = mapToScene(cme->pos());

    QList<QGraphicsItem*> items = scene()->items(scenePt);
    if(items.count())
    {
        QGraphicsItem* firstItem = items[0];

        QMenu contextMenu(this);

        QAction columnAction1("Column Item 1", this);
        QAction tableAction1("Table Item 1", this);
        QAction tableHeaderAction1("Table header 1", this);

        switch (firstItem->type())
        {
        case COLUMN_TYPE:
            contextMenu.addAction(&columnAction1);
            break;

        case TABLE_TYPE:
            contextMenu.addAction(&tableAction1);
            break;

        case TABLE_HEADER_TYPE:
            contextMenu.addAction(&tableHeaderAction1);
            break;
        }

        contextMenu.exec(cme->globalPos());
    }
}


void DbGraphicsView::wheelEvent(QWheelEvent *we)
{
    if (we->modifiers() & Qt::ControlModifier)
    {
        // Do a wheel-based zoom about the cursor position
        double angle = we->angleDelta().y();
        double factor = qPow(1.0015, angle);

        auto targetViewportPos = we->position().toPoint();
        auto targetScenePos = mapToScene(we->position().toPoint());

        scale(factor, factor);
        centerOn(targetScenePos);
        QPointF deltaViewportPos = targetViewportPos - QPointF(viewport()->width() / 2.0, viewport()->height() / 2.0);
        QPointF viewportCenter = mapFromScene(targetScenePos) - deltaViewportPos;
        centerOn(mapToScene(viewportCenter.toPoint()));

        return;
    }
}
