#include "add_connection_dialog.h"

AddConnectionDialog::AddConnectionDialog(QWidget* parent) :
    QDialog(parent)
{
    setupUi(this);
}


void AddConnectionDialog::on_trustedConnectionCheck_stateChanged(int state)
{
    userNameEdit->setEnabled(state == Qt::Unchecked);
    passwordEdit->setEnabled(state == Qt::Unchecked);
}
