#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

protected slots:
    void on_pushButton_clicked();
    void on_explorerTreeView_customContextMenuRequested(const QPoint&);
    void on_actionAddConnection_triggered();

protected:
    //QGraphicsScene* scene_;
};

#endif // MAINWINDOW_H
