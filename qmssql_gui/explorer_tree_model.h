#ifndef EXPLORER_TREE_MODEL_H
#define EXPLORER_TREE_MODEL_H


#include <QList>
#include <QAbstractItemModel>
#include <QString>

#include "db_struct.h"


enum class ExplorerItemType
{
    ROOT_ITEM,
    DB_NAME,
    TABLES,
    TABLE_NAME,
    COLUMNS,
    COLUMN_NAME,
    INDEXES,
    INDEX_NAME
};


class ExplorerTreeItem
{
public:
    ExplorerTreeItem(ExplorerItemType type, const QVector<QVariant>& data, ExplorerTreeItem* parent = nullptr);
    virtual ~ExplorerTreeItem();

    ExplorerItemType type() const;
    virtual QVariant data(int column) const;

    void addChild(ExplorerTreeItem* child);
    ExplorerTreeItem* child(int row) const;
    int childCount() const;
    int columnCount() const;
    int row() const;
    ExplorerTreeItem* parent();

protected:
    ExplorerItemType _type;
    ExplorerTreeItem* _parent;
    QList<ExplorerTreeItem*> _children;

    QVector<QVariant> _data;
};



class ExplorerTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    ExplorerTreeModel(QObject* parent = nullptr);
    virtual ~ExplorerTreeModel();

    void addDatabase(const Database& dbObj);

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

protected:
    ExplorerTreeItem* _rootItem;
};


#endif // EXPLORER_TREE_MODEL_H


