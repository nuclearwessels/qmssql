#ifndef ADDCONNECTIONDIALOGIMPL_H
#define ADDCONNECTIONDIALOGIMPL_H

#include <QDialog>
#include "ui_add_connection_dialog.h"


class AddConnectionDialog : public QDialog, private Ui::AddConnectionDialog
{
    Q_OBJECT

public:
    AddConnectionDialog(QWidget* parent = nullptr);


    bool trustedConnection() const;
    bool encryptConnection() const;


protected slots:
    void on_trustedConnectionCheck_stateChanged(int state);

};

#endif // ADDCONNECTIONDIALOGIMPL_H
