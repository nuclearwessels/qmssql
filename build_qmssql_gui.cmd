@echo off

cd qmssql_gui

qmake qmssql_gui.pro
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

mingw32-make -f Makefile.Release
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

cd ..


mkdir dist
mkdir dist\qmssql_gui
copy qmssql_gui\release\qmssql_gui.exe dist\qmssql_gui
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

windeployqt dist\qmssql_gui\qmssql_gui.exe
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

