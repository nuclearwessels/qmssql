

#include "db_struct.h"

#include <QRegularExpression>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include <QDebug>

#include <algorithm>



const QMap<Difference::DiffType, QString> Difference::DIFF_TYPE_STRINGS =
{
    std::pair<DiffType, QString>(ADD, "Add"),
    std::pair<DiffType, QString>(MODIFY, "Modify"),
    std::pair<DiffType, QString>(REMOVE, "Remove")
};


const QMap<Difference::ObjectType, QString> Difference::OBJECT_TYPE_STRINGS =
{
    std::pair<ObjectType, QString>(TABLE, "Table"),
    std::pair<ObjectType, QString>(TABLE_COLUMN, "Table Column"),
    std::pair<ObjectType, QString>(INDEX, "Index")
};


Difference::Difference(DiffType diffType, ObjectType objType, const QString& name) :
    diffType_(diffType),
    objectType_(objType),
    name_(name)
{

}

Difference::~Difference()
{

}


Difference::DiffType Difference::diffType() const
{
    return diffType_;
}


Difference::ObjectType Difference::objectType() const
{
    return objectType_;
}

const QString& Difference::name() const
{
    return name_;
}


QList<QString>& Difference::statements()
{
    return statements_;
}


const QList<QString>& Difference::statements() const
{
    return statements_;
}


QString Difference::toString() const
{
    QString s = QString("Diff type: %1, Object type: %2, name: %3\n").arg(DIFF_TYPE_STRINGS[diffType_])
                                                                     .arg(OBJECT_TYPE_STRINGS[objectType_])
                                                                     .arg(name_);

    if (statements_.length())
    {
        s += "Statements: \n";

        for (int i=0; i<statements_.length(); i++)
        {
            QString statement = QString("%1: %2\n").arg(i+1).arg(statements_.at(i));
            s += statement;
        }
    }

    return s;
}



TableDifference::TableDifference(DiffType diffType, const QString& name) :
    Difference(diffType, TABLE, name)
{

}


ColumnDifference::ColumnDifference(DiffType diffType, const QString& tableName, const QString& columnName) :
    Difference(diffType, TABLE_COLUMN, tableName + "_" + columnName)
{

}



DatabaseObject::DatabaseObject() :
    valid_(false)
{

}


DatabaseObject::DatabaseObject(const QString& name) :
    name_(name),
    valid_(true)
{

}


//DatabaseObject::DatabaseObject(const DatabaseObject& obj)
//{
//    *this = obj;
//}


//DatabaseObject& DatabaseObject::operator = (const DatabaseObject& obj)
//{
//    name_ = obj.name_;
//    valid_ = obj.valid_;

//    return *this;
//}


DatabaseObject::~DatabaseObject()
{

}


const QString& DatabaseObject::name() const
{
    Q_ASSERT(valid_);
    return name_;
}


bool DatabaseObject::valid() const
{
    return valid_;
}


QList<QString> DatabaseObject::createStatements() const
{
    return QList<QString>();
}

QList<QString> DatabaseObject::alterStatements() const
{
    return QList<QString>();
}

QList<QString> DatabaseObject::dropStatements() const
{
    return QList<QString>();
}


IndexColumn::IndexColumn(const QString& name, int columnId, bool descending, bool included) :
    DatabaseObject(name),
    columnId_(columnId),
    descending_(descending),
    included_(included)
{

}


int IndexColumn::columnId() const
{
    return columnId_;
}

bool IndexColumn::descending() const
{
    return descending_;
}

bool IndexColumn::included() const
{
    return included_;
}



Index::Index(const QString& name, IndexType type, bool primaryKey, bool unique,
             int fillFactor, bool filter, const QString& filterDefinition,
             bool allowRowLocks, bool allowPageLocks, bool ignoreDupKey) :
    DatabaseObject(name),
    type_(type),
    primaryKey_(primaryKey),
    unique_(unique),
    fillFactor_(fillFactor),
    filter_(filter),
    filterDefinition_(filterDefinition),
    allowRowLocks_(allowRowLocks),
    allowPageLocks_(allowPageLocks),
    ignoreDupKey_(ignoreDupKey)
{

}


bool Index::primaryKey() const
{
    return primaryKey_;
}

bool Index::unique() const
{
    return unique_;
}

bool Index::filter() const
{
    return filter_;
}

const QString& Index::filterDefinition() const
{
    return filterDefinition_;
}

int Index::fillFactor() const
{
    return fillFactor_;
}



QList<IndexColumn>& Index::columns()
{
    return columns_;
}

const QList<IndexColumn>& Index::columns() const
{
    return columns_;
}



ForeignKey::ForeignKey(const QString& name, const QString& refTable, const QString& refColumn) :
    DatabaseObject(name),
    refTable_(refTable),
    refColumn_(refColumn)
{

}

const QString& ForeignKey::referencedTable() const
{
    return refTable_;
}

const QString& ForeignKey::referencedColumn() const
{
    return refColumn_;
}



DefaultConstraint::DefaultConstraint() : DatabaseObject()
{

}


DefaultConstraint::DefaultConstraint(const QString& name, const QString& definition) :
    DatabaseObject(name),
    definition_(definition)
{

}


const QString& DefaultConstraint::definition() const
{
    return definition_;
}





TableColumn::TableColumn(const QString& name, int columnId,
                         bool sparse, bool nullable, bool identity, bool computed,
                         DataType dataType, int length, int precision, int scale,
                         const QString& collation,
                         const QString& computedExpression,
                         bool computedPersisted) :
    DatabaseObject(name),
    columnId_(columnId),
    sparse_(sparse),
    nullable_(nullable),
    identity_(identity),
    computed_(computed),
    dataType_(dataType),
    length_(length),
    precision_(precision),
    scale_(scale),
    collation_(collation),
    computedExpression_(computedExpression),
    computedPersisted_(computedPersisted)
{

}


bool TableColumn::operator == (const TableColumn& tc) const
{
    if (this->name().compare(tc.name(), Qt::CaseInsensitive) != 0)
        return false;

    if (this->dataType_ != tc.dataType_)
        return false;

    if (this->length_ != tc.length_)
        return false;

    if (this->precision_ != tc.precision_)
        return false;

    if (this->scale_ != tc.scale_)
        return false;

    if (this->nullable_ != tc.nullable_)
        return false;

    if (this->identity_ != tc.identity_)
        return false;

    if (this->computed_ != tc.computed_)
        return false;

    if (this->computedExpression_.compare(tc.computedExpression_) != 0)
        return false;

    return true;
}


void TableColumn::setDefaultConstraint(const DefaultConstraint& dc)
{
    defaultConstraint_ = dc;
}

const DefaultConstraint& TableColumn::defaultConstraint() const
{
    return defaultConstraint_;
}


QList<ForeignKey>& TableColumn::foreignKeys()
{
    return foreignKeys_;
}

const QList<ForeignKey>& TableColumn::foreignKeys() const
{
    return foreignKeys_;
}


QString TableColumn::createTableFragment() const
{
    QString fragment;

    fragment.append(name());
    fragment.append(" ");

    if (computed_)
    {
        fragment.append("AS ");
        fragment.append(computedExpression_);
        fragment.append(" ");

        if (computedPersisted_)
            fragment.append("PERSISTED ");
    }
    else
    {
        // add the data type string
        QString dataTypeString;
        for (auto i = DATATYPE_STRINGS.cbegin(); i != DATATYPE_STRINGS.cend(); i++)
        {
            if (i.value() == dataType_)
            {
                dataTypeString = i.key();
                break;
            }
        }

        fragment.append(dataTypeString);

        // add the length of the field if appropriate
        switch (dataType_)
        {
        case BINARY:
        case CHAR:
        case VARBINARY:
        case VARCHAR:
            if (length_ == 4000)
                fragment.append("(max)");
            else
                fragment.append(QString("(%1)").arg(length_));
            break;

        case NVARCHAR:
            if (length_ == 8000)
                fragment.append("max");
            else
                fragment.append(QString("(%1)").arg(length_ / 2));
            break;

        case BIGINT:
        case BIT:
        case REAL:
        case DATE:
        case DATETIME:
        case DATETIME2:
        case FLOAT:
        case HIERARCHYID:
        case INT:
        case SMALLDATETIME:
        case SMALLINT:
        case TINYINT:
        case UNIQUEIDENTIFIER:
            // nothing to do
            break;
        }

        fragment.append(" ");

        if (identity_)
        {
            fragment.append("IDENTITY ");
        }
        else if (!nullable_)
        {
            fragment.append("NOT NULL ");
        }

        if (sparse_)
            fragment.append("SPARSE ");

        if (defaultConstraint_.valid())
        {
            fragment.append("DEFAULT ");
            fragment.append(defaultConstraint_.definition());
            fragment.append(" ");
        }
    }

    return fragment;
}




const QMap<QString, TableColumn::DataType> TableColumn::DATATYPE_STRINGS =
{
    { "bigint", BIGINT },
    { "binary", BINARY },
    { "bit",    BIT },
    { "char",   CHAR },
    { "date",   DATE },
    { "datetime",       DATETIME },
    { "datetime2",      DATETIME2 },
    { "decimal",        DECIMAL },
    { "float",          FLOAT },
    { "hierarchyid",    HIERARCHYID },
    { "int",            INT },
    { "nchar",          NCHAR },
    { "nvarchar",       NVARCHAR },
    { "real",           REAL },
    { "smalldatetime",  SMALLDATETIME },
    { "smallint",       SMALLINT },
    { "tinyint",        TINYINT },
    { "uniqueidentifier",   UNIQUEIDENTIFIER },
    { "varbinary",          VARBINARY },
    { "varchar",            VARCHAR },
    { "xml",                XML }
};


TableColumn::DataType TableColumn::FindDataType(const QString& dataType)
{
    if (DATATYPE_STRINGS.contains(dataType))
    {
        return DATATYPE_STRINGS[dataType];
    }

    Q_ASSERT(false);

    return static_cast<TableColumn::DataType>(0);
}



const QStringList Table::RESERVED_TABLES =
{
    "sysdiagrams"
};




Table::Table(const QString& tableName) :
    DatabaseObject(tableName)
{

}


QList<TableColumn>& Table::columns()
{
    return columns_;
}

const QList<TableColumn>& Table::columns() const
{
    return columns_;
}


QList<Index>& Table::indexes()
{
    return indexes_;
}

const QList<Index>& Table::indexes() const
{
    return indexes_;
}


QList<QString> Table::createStatements() const
{
    // create the CREATE TABLE statement
    QList<QString> statements;

    // first build up the columns
    QList<QString> createTableFragments;
    for (const TableColumn& tc : columns_)
    {
        QString fragment = tc.createTableFragment();
        createTableFragments.append(fragment);
    }

    // now look for primary key indexes and create a constraint
    for (const Index& idx : indexes_)
    {
        if (idx.primaryKey())
        {
            // add the primary key constraint
            QString fragment = "CONSTRAINT ";
            fragment.append(idx.name());
            fragment.append(" PRIMARY KEY ");

            QStringList colNames;
            for (const IndexColumn& idxcol : idx.columns())
            {
                colNames.append(idxcol.name());
            }

            fragment.append("(");
            fragment.append( colNames.join(",") );
            fragment.append(")");

            createTableFragments.append(fragment);
        }
    }

    QString joined = createTableFragments.join(",\n");

    QString createTableStatement = "CREATE TABLE " + name() + " (\n";
    createTableStatement.append(joined);
    createTableStatement.append(");");

    statements.append(createTableStatement);

    return statements;
}


QList<QString> Table::dropStatements() const
{
    QString dropTableStatement = "DROP TABLE " + name() + ";";

    QList<QString> statements;
    statements.append(dropTableStatement);

    return statements;
}


QList<Difference> Table::difference(const Table& t1, const Table& t2)
{
    QList<Difference> diffs;

    // make sure all of the columns in table 1 are in table 2
    for (const auto& col1 : t1.columns())
    {
        auto col2Iter = std::find_if(t2.columns().cbegin(), t2.columns().cend(),
            [&](const TableColumn& tc)
            {
                return tc.name().compare(col1.name(), Qt::CaseInsensitive) == 0;
            });

        if (col2Iter == t2.columns().end())
        {
            // add the column
            ColumnDifference cd(Difference::ADD, t1.name(), col1.name());
            cd.statements().append(col1.createStatements());

            diffs.append(cd);
        }
    }

    // look for columns in table 2 that aren't in table 1 and drop them
    for (const auto& col2 : t2.columns())
    {
        auto col1Iter = std::find_if(t1.columns().cbegin(), t1.columns().cend(),
            [&](const TableColumn& tc) -> bool
            {
                return tc.name().compare(col2.name(), Qt::CaseInsensitive) == 0;
            });

        if (col1Iter == t1.columns().cend())
        {
            // drop the column
            ColumnDifference cd(Difference::REMOVE, t2.name(), col2.name());
            cd.statements().append(col2.dropStatements());

            diffs.append(cd);
        }

    }

    return diffs;
}



Database::Database(const QString& connString) :
    connString_(connString)
{
    // parse the name of the database from the connection string
    QRegularExpression regex("Database=(\\S*);", QRegularExpression::CaseInsensitiveOption);

    QRegularExpressionMatch match = regex.match(connString);
    if (match.hasMatch())
    {
        name_ = match.captured(1);
    }

    db_ = QSqlDatabase::addDatabase("QODBC", name_);
    db_.setDatabaseName(connString);

    bool ok = db_.open();
    Q_ASSERT(ok);
}



Database::Database(const QString& name, const QString& connString) :
    connString_(connString),
    name_(name)
{
    db_ = QSqlDatabase::addDatabase("QODBC", name);
    db_.setDatabaseName(connString);

    bool ok = db_.open();
    Q_ASSERT(ok);
}


Database::~Database()
{
    db_.close();
}


const QString& Database::name() const
{
    return name_;
}

bool Database::readAll()
{
    tables_.clear();

    bool ok = readTables();
    ok &= readTableColumns();
    ok &= readIndexes();
    ok &= readIndexColumns();
    ok &= readForeignKeys();
    ok &= readDefaultConstraints();

    return ok;
}



bool Database::readTables()
{
    const QString QUERY_TEXT = R"(SELECT Name FROM sys.tables)";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    tables_.clear();
    while (query.next())
    {
        QString tableName = query.record().value(0).toString();
        qDebug() << tableName;

        if (!Table::RESERVED_TABLES.contains(tableName, Qt::CaseInsensitive))
        {
            Table table(tableName);
            tables_.append(table);
        }
    }

    return true;
}


bool Database::readTableColumns()
{
    const QString QUERY_TEXT =
R"***(
SELECT t.name, c.name as col_name, c.column_id, c.collation_name,
c.max_length, c.precision, c.scale, c.is_nullable, c.is_identity, c.is_computed, c.is_sparse,
cc.definition,
cc.is_persisted,
types.Name as data_type
FROM sys.tables t
INNER JOIN sys.columns c ON c.object_id = t.object_id
INNER JOIN sys.types ON types.user_type_id = c.user_type_id
LEFT JOIN sys.computed_columns cc ON cc.column_id = c.column_id AND cc.object_id = c.object_id
ORDER BY t.name, c.column_id;
)***";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    // clear all of the table columns before reading out the data
    for (auto& table : tables_)
    {
        table.columns().clear();
    }

    while (query.next())
    {
        int col = 0;
        QString tableName = query.record().value(col++).toString();
        QString columnName = query.record().value(col++).toString();
        int columnId = query.record().value(col++).toInt();
        QString collationName = query.record().value(col++).toString();
        int maxLength = query.record().value(col++).toInt();
        int precision = query.record().value(col++).toInt();
        int scale = query.record().value(col++).toInt();
        bool isNullable = query.record().value(col++).toBool();
        bool isIdentity = query.record().value(col++).toBool();
        bool isComputed = query.record().value(col++).toBool();
        bool isSparse = query.record().value(col++).toBool();
        QString computedDefinition = query.record().value(col++).toString();
        bool isPersisted = query.record().value(col++).toBool();
        QString dataTypeName = query.record().value(col++).toString();

        qDebug() << tableName << ": " << columnName;

        if (Table::RESERVED_TABLES.contains(tableName, Qt::CaseInsensitive))
            continue;

        // find the table
        QList<Table>::iterator tableIter = std::find_if(tables_.begin(), tables_.end(),
        [&](const Table& t)
        {
            return t.name() == tableName;
        });

        Q_ASSERT(tableIter != tables_.end());

        TableColumn::DataType dataType = TableColumn::FindDataType(dataTypeName);


        Table& table = *tableIter;

        // now add the column
        TableColumn column(columnName, columnId, isSparse, isNullable, isIdentity, isComputed,
                           dataType, maxLength, precision, scale,
                           collationName, computedDefinition, isPersisted);

        table.columns().append(column);
    }

    return true;
}


bool Database::readIndexes()
{
    const QString QUERY_TEXT =
R"***(
    SELECT t.name, idx.name, idx.type, idx.type_desc, idx.is_primary_key, idx.is_unique,
      idx.fill_factor, idx.has_filter, idx.filter_definition, idx.allow_row_locks, idx.allow_page_locks,
      idx.ignore_dup_key  FROM sys.tables t
      INNER JOIN sys.indexes idx ON idx.object_id = t.object_id
      ORDER BY t.name, idx.is_primary_key DESC, idx.name;
)***";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    // clear the indexes for each table
    for (Table& table : tables_)
    {
        table.indexes().clear();
    }

    while (query.next())
    {
        int col = 0;
        QString tableName = query.record().value(col++).toString();
        QString indexName = query.record().value(col++).toString();
        Index::IndexType indexType = static_cast<Index::IndexType>(query.record().value(col++).toInt());
        col++;  // skip the index type description
        bool isPrimaryKey = query.record().value(col++).toBool();
        bool isUnique = query.record().value(col++).toBool();
        int fillFactor = query.record().value(col++).toInt();
        bool hasFilter = query.record().value(col++).toBool();
        QString filterDefinition = query.record().value(col++).toString();
        bool allowRowLocks = query.record().value(col++).toBool();
        bool allowPageLocks = query.record().value(col++).toBool();
        bool ignoreDupKey = query.record().value(col++).toBool();

        if (Table::RESERVED_TABLES.contains(tableName, Qt::CaseInsensitive))
            continue;

        // find the table
        QList<Table>::iterator tableIter = std::find_if(tables_.begin(), tables_.end(),
        [&](const Table& t)
        {
            return t.name() == tableName;
        });
        Q_ASSERT(tableIter != tables_.end());

        Table& table = *tableIter;

        Index idx(indexName, indexType, isPrimaryKey, isUnique, fillFactor,
                  hasFilter, filterDefinition,
                  allowRowLocks, allowPageLocks, ignoreDupKey);
        table.indexes().append(idx);
    }

    return true;
}



bool Database::readIndexColumns()
{
    const QString QUERY_TEXT =
R"***(
    SELECT
         TableName = t.name,
         IndexName = ind.name,
         IndexId = ind.index_id,
         ColumnId = ic.index_column_id,
         ColumnName = col.name,
         ic.is_descending_key,
         ic.is_included_column
    FROM sys.indexes ind
    INNER JOIN sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id
    INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id
    INNER JOIN sys.tables t ON ind.object_id = t.object_id
    ORDER BY
         t.name, ind.name, ind.index_id, ic.index_column_id;
)***";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    while (query.next())
    {
        QString tableName = query.record().value(0).toString();
        QString indexName = query.record().value(1).toString();
        //int indexId = query.record().value(2).toInt();
        int columnId = query.record().value(3).toInt();
        QString columnName = query.record().value(4).toString();
        bool isDescendingKey = query.record().value(5).toBool();
        bool isIncludedColumn = query.record().value(6).toBool();

        if (Table::RESERVED_TABLES.contains(tableName, Qt::CaseInsensitive))
            continue;

        // find the right table
        QList<Table>::iterator tableIter = std::find_if(tables_.begin(), tables_.end(),
            [&](const Table& table)
            {
                return table.name() == tableName;
            });
        Q_ASSERT(tableIter != tables_.end());
        if (tableIter == tables_.end())
            return false;

        Table& table = *tableIter;

        // now find the index
        QList<Index>::iterator indexIter = std::find_if(table.indexes().begin(), table.indexes().end(),
            [&](const Index& index)
            {
                return index.name() == indexName;
            });
        Q_ASSERT(indexIter != table.indexes().end());
        if (indexIter == table.indexes().end())
            return false;

        Index& index = *indexIter;

        // now add the index column to the index
        IndexColumn idxcol(columnName, columnId, isDescendingKey, isIncludedColumn);
        index.columns().append(idxcol);
    }

    return true;
}



bool Database::readForeignKeys()
{
    const QString QUERY_TEXT =
R"***(
    SELECT
        fk.name,
        t.name 'Parent table',
        c1.name 'Parent column',
        t2.name 'Referenced table',
        c2.name 'Referenced column'
    FROM
        sys.foreign_keys fk
    INNER JOIN sys.tables t ON t.object_id = fk.parent_object_id
    LEFT JOIN sys.tables t2 ON t2.object_id = fk.referenced_object_id
    INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
    INNER JOIN sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
    INNER JOIN sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id;
)***";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    while (query.next())
    {
        QString foreignKeyName = query.record().value(0).toString();
        QString parentTableName = query.record().value(1).toString();
        QString parentColumn = query.record().value(2).toString();
        QString referencedTableName = query.record().value(3).toString();
        QString referencedColumn = query.record().value(4).toString();

        // find the right table
        QList<Table>::iterator tableIter = std::find_if(tables_.begin(), tables_.end(),
            [&](const Table& table)
            {
                return table.name() == parentTableName;
            });

        if (tableIter == tables_.end())
            return false;

        Table& parentTable = *tableIter;

        // now find the column
        QList<TableColumn>::iterator colIter = std::find_if(parentTable.columns().begin(), parentTable.columns().end(),
            [&](const TableColumn& tc)
            {
                return tc.name() == parentColumn;
            });
        if (colIter == parentTable.columns().end())
            return false;

        TableColumn& col = *colIter;

        ForeignKey fk(foreignKeyName, referencedTableName, referencedColumn);
        col.foreignKeys().append(fk);
    }

    return true;
}



bool Database::readDefaultConstraints()
{
    const QString QUERY_TEXT =
R"***(
    SELECT
        TableName = t.Name,
        ColumnName = c.Name,
        dc.Name,
        dc.definition
    FROM sys.tables t
    INNER JOIN sys.default_constraints dc ON t.object_id = dc.parent_object_id
    INNER JOIN sys.columns c ON dc.parent_object_id = c.object_id AND c.column_id = dc.parent_column_id
    ORDER BY t.Name;
)***";

    QSqlQuery query(db_);
    query.setForwardOnly(true);
    bool ok = query.exec(QUERY_TEXT);
    if (!ok)
        return false;

    while (query.next())
    {
        QString tableName = query.record().value(0).toString();
        QString columnName = query.record().value(1).toString();
        QString dcName = query.record().value(2).toString();
        QString dcDef = query.record().value(3).toString();

        // find the right table
        QList<Table>::iterator tableIter = std::find_if(tables_.begin(), tables_.end(),
            [&](const Table& table)
            {
                return table.name() == tableName;
            });
        if (tableIter == tables_.end())
            return false;

        Table& table = *tableIter;

        // now find the column
        QList<TableColumn>::iterator colIter = std::find_if(table.columns().begin(), table.columns().end(),
            [&](const TableColumn& tc)
            {
                return tc.name() == columnName;
            });
        if (colIter == table.columns().end())
            return false;

        TableColumn& col = *colIter;

        DefaultConstraint dc(dcName, dcDef);
        col.setDefaultConstraint(dc);
    }

    return true;
}


const QList<Table>& Database::tables() const
{
    return tables_;
}


QList<Difference> Database::difference(const Database& db1, const Database& db2)
{
    QList<Difference> dbDiffs;

    // we want to make db2 like db1

    QList<Difference> diffs = createTableDifference(db1, db2);
    dbDiffs.append(diffs);

    diffs = dropTableDifference(db1, db2);
    dbDiffs.append(diffs);

    // now look for column differences in tables that exist in both dbs
    for (const Table& t1 : db1.tables())
    {
        // find the table of the same name in t2
        auto t2Iter = std::find_if(db2.tables().cbegin(), db2.tables().cend(),
            [&](const Table& table2) -> bool
            {
                return t1.name().compare(table2.name(), Qt::CaseInsensitive) == 0;
            });

        if (t2Iter != db2.tables().cend())
        {
            QList<Difference> diffs = Table::difference(t1, *t2Iter);
            dbDiffs.append(diffs);
        }

    }

    return dbDiffs;
}


QList<Difference> Database::createTableDifference(const Database& db1, const Database& db2)
{
    QList<Difference> diffs;

    // find any tables in db1 that aren't in db2
    QList<QString> db2MissingTables;
    for (const Table& t : db1.tables())
    {
        db2MissingTables.append(t.name());
    }

    for (const Table& t : db2.tables())
    {
        db2MissingTables.removeAll(t.name());
    }

    // now create the missing tables
    for (const Table& t : db1.tables())
    {
        if (db2MissingTables.contains(t.name()))
        {
            TableDifference tableDiff(Difference::ADD, t.name());

            QList<QString> createStatements = t.createStatements();

            tableDiff.statements().append( createStatements );

            diffs.append(tableDiff);
        }
    }

    return diffs;
}


QList<Difference> Database::dropTableDifference(const Database& db1, const Database& db2)
{
    qDebug() << "Looking for tables to drop between " << db1.name() << " and " << db2.name();

    QList<Difference> diffs;

    for (const Table& table2 : db2.tables())
    {
        qDebug() << table2.name();

        // see if the table exists in db1
        auto tableIter = std::find_if(db1.tables().cbegin(), db1.tables().cend(),
            [&](const Table& t)
        {
            return (table2.name().compare(t.name(), Qt::CaseInsensitive) == 0);
        });

        if (tableIter == db1.tables().cend())
        {
            qDebug() << "Table " << table2.name() << " doesn't exist in " << db1.name();

            // table isn't found in db2, so drop it
            TableDifference tableDiff(Difference::REMOVE, table2.name());
            QList<QString> dropStatements = table2.dropStatements();
            tableDiff.statements().append(dropStatements);
            diffs.append(tableDiff);
        }
    }

    return diffs;
}
