#ifndef DB_STRUCT_H
#define DB_STRUCT_H

#include <QList>
#include <QString>
#include <QSqlDatabase>



class Difference
{
public:
    virtual ~Difference();

    enum DiffType
    {
        ADD,
        MODIFY,
        REMOVE
    };

    enum ObjectType
    {
        TABLE,
        TABLE_COLUMN,
        INDEX
    };

    DiffType diffType() const;
    ObjectType objectType() const;
    const QString& name() const;

    QList<QString>& statements();
    const QList<QString>& statements() const;

    virtual QString toString() const;

protected:
    Difference(DiffType dt, ObjectType ot, const QString& name);

    DiffType diffType_;
    ObjectType objectType_;
    QString name_;
    QList<QString> statements_;

    static const QMap<DiffType, QString> DIFF_TYPE_STRINGS;
    static const QMap<ObjectType, QString> OBJECT_TYPE_STRINGS;
};


class TableDifference : public Difference
{
public:
    TableDifference(DiffType diffType, const QString& tableName);


};


class ColumnDifference : public Difference
{
public:
    ColumnDifference(DiffType diffType, const QString& tableName, const QString& columnName);

private:

};


class IndexDifference : public Difference
{
public:
    IndexDifference(DiffType diffType, const QString& indexName);

private:

};


class DatabaseObject
{
public:
    DatabaseObject();
    DatabaseObject(const QString& name);
    virtual ~DatabaseObject();

    const QString& name() const;

    bool valid() const;

    virtual QList<QString> createStatements() const;
    virtual QList<QString> alterStatements() const;
    virtual QList<QString> dropStatements() const;

private:
    QString name_;
    bool valid_;
};



class IndexColumn : public DatabaseObject
{
public:
    IndexColumn(const QString& name, int columnId, bool descending, bool included);

    int columnId() const;
    bool descending() const;
    bool included() const;

private:
    int columnId_;
    bool descending_;
    bool included_;
};


class Index : public DatabaseObject
{
public:
    enum IndexType
    {
        HEAP = 0,
        CLUSTERED = 1,
        NONCLUSTERED = 2
    };

    Index(const QString& name, IndexType type, bool primaryKey, bool unique,
          int fillFactor, bool filter, const QString& filterDefinition,
          bool allowRowLocks, bool allowPageLocks, bool ignoreDupKey);

    bool primaryKey() const;
    bool unique() const;
    bool filter() const;
    const QString& filterDefinition() const;
    int fillFactor() const;

    QList<IndexColumn>& columns();
    const QList<IndexColumn>& columns() const;

    static QList<Difference> difference(const Index& idx1, const Index& idx2);

private:
    IndexType type_;
    bool primaryKey_;
    bool unique_;
    int fillFactor_;
    bool filter_;
    QString filterDefinition_;
    bool allowRowLocks_;
    bool allowPageLocks_;
    bool ignoreDupKey_;

    QList<IndexColumn> columns_;
};


class ForeignKey : public DatabaseObject
{
public:
    ForeignKey(const QString& name, const QString& refTable, const QString& refColumn);

    const QString& referencedTable() const;
    const QString& referencedColumn() const;

private:
    QString refTable_;
    QString refColumn_;
};


class DefaultConstraint : public DatabaseObject
{
public:
    DefaultConstraint();
    DefaultConstraint(const QString& name, const QString& definition);

    const QString& definition() const;

private:
    QString definition_;
};


class TableColumn : public DatabaseObject
{
public:
    enum DataType
    {
        BIGINT,
        BINARY,
        BIT,
        CHAR,
        DATE,
        DATETIME,
        DATETIME2,
        DECIMAL,
        FLOAT,
        HIERARCHYID,
        INT,
        NCHAR,
        NVARCHAR,
        REAL,
        SMALLDATETIME,
        SMALLINT,
        TINYINT,
        UNIQUEIDENTIFIER,
        VARBINARY,
        VARCHAR,
        XML
    };

    TableColumn();
    TableColumn(const QString& name, int columnId,
                bool sparse, bool nullable, bool identity, bool computed,
                DataType dataType, int length, int precision, int scale,
                const QString& collation = QString(),
                const QString& computedExpression = QString(),
                bool computedPersisted = false);

    bool operator == (const TableColumn& tc) const;

    void setDefaultConstraint(const DefaultConstraint& dc);
    const DefaultConstraint& defaultConstraint() const;

    QList<ForeignKey>& foreignKeys();
    const QList<ForeignKey>& foreignKeys() const;

    QString createTableFragment() const;

    static const QMap<QString, DataType> DATATYPE_STRINGS;
    static DataType FindDataType(const QString& dataType);

private:
    int columnId_;
    bool sparse_;
    bool nullable_;
    bool identity_;
    bool computed_;
    DataType dataType_;
    int length_;
    int precision_;
    int scale_;
    QString collation_;

    QString computedExpression_;
    bool computedPersisted_;

    DefaultConstraint defaultConstraint_;

    QList<ForeignKey> foreignKeys_;
};


class Table : public DatabaseObject
{
public:
    Table();
    Table(const QString& name);

    bool operator == (const Table& tb) const;

    QList<TableColumn>& columns();
    const QList<TableColumn>& columns() const;

    QList<Index>& indexes();
    const QList<Index>& indexes() const;

    QList<QString> createStatements() const override;
    QList<QString> dropStatements() const override;

    static QList<Difference> difference(const Table& t1, const Table& t2);

    static const QStringList RESERVED_TABLES;

private:
    QList<TableColumn> columns_;
    QList<Index> indexes_;
};





class Database
{
public:
    Database(const QString& connString);
    Database(const QString& name, const QString& connString);
    ~Database();

    const QString& name() const;

    bool readAll();

    bool readTables();
    bool readTableColumns();
    bool readIndexes();
    bool readIndexColumns();

    bool readDefaultConstraints();
    bool readForeignKeys();

    const QList<Table>& tables() const;

    static QList<Difference> difference(const Database& db1, const Database& db2);

private:
    QString connString_;
    QString name_;

    QSqlDatabase db_;

    QList<Table> tables_;

    static QList<Difference> createTableDifference(const Database& db1, const Database& db2);
    static QList<Difference> dropTableDifference(const Database& db1, const Database& db2);


};


#endif // DB_STRUCT_H
