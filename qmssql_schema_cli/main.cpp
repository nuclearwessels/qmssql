#include <QCoreApplication>
#include <QDebug>
#include <iostream>

#include "db_struct.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString connString1(a.arguments().at(1));
    QString connString2(a.arguments().at(2));


    Database db1(connString1);
    bool ok = db1.readTables();
    Q_ASSERT(ok);

    ok = db1.readTableColumns();
    Q_ASSERT(ok);

    ok = db1.readIndexes();
    Q_ASSERT(ok);

    ok = db1.readIndexColumns();
    Q_ASSERT(ok);

    ok = db1.readForeignKeys();
    Q_ASSERT(ok);

    ok = db1.readDefaultConstraints();
    Q_ASSERT(ok);


    Database db2(connString2);
    ok = db2.readTables();
    Q_ASSERT(ok);

    ok = db2.readTableColumns();
    Q_ASSERT(ok);

    ok = db2.readIndexes();
    Q_ASSERT(ok);

    ok = db2.readIndexColumns();
    Q_ASSERT(ok);

    ok = db2.readForeignKeys();
    Q_ASSERT(ok);

    ok = db2.readDefaultConstraints();
    Q_ASSERT(ok);


    QList<Difference> diffs = Database::difference(db2, db1);

    for (const auto& diff : qAsConst(diffs))
    {
        std::cout << diff.toString().toStdString() << std::endl;
    }

    return a.exec();
}
